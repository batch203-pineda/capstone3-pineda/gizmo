import Carousel from 'react-bootstrap/Carousel';

export default function Banner() {
  return (
    <>
    <Carousel style={{ width: '100%' }}>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://images.unsplash.com/photo-1519458246479-6acae7536988?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80?text=First slide&bg=373940 "
          alt="First slide"
        />
        <Carousel.Caption>
          <p>“When words become unclear, I shall focus with photographs. When images become inadequate, I shall be content with silence.”</p>
          <p>– Ansel Adams</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://images.pexels.com/photos/2929411/pexels-photo-2929411.jpeg?text=Second slide&bg=282c34"
          alt="Second slide"
        />

        <Carousel.Caption>
          <p>“Photography is the art of making memories tangible.”</p>
          <p>– Destin Sparks</p>
        </Carousel.Caption>
      </Carousel.Item>
      
    </Carousel>
    </>
    
  );
}
