import { useState, useEffect } from "react";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import { Card, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";


								// destructure the "courseProp" from the prop parameter
								// CourseCard(prop)
export default function ProductCard({productProp}) {
	
	// console.log(props.courseProp.name);
	// console.log(typeof props);
	// console.log(courseProp);

	// Scenario: Keep track the number of enrollees of each course.

	// Destructure the course properties into their own variables
	const { _id, productName, description, price, availableStock } = productProp;

	// Syntax:
		// const [stateName, setStateName] = useState(initialStateValue);
			// Using the state hook, it returns an array with the following elements:
				// first element contains the the current inital State value.
				// second element is a setter function that is used to change the value of the first element.

		// const [count, setCount] = useState(0);
		// console.log(useState(10));

		// Use state hook for getting the seats for this product.
		/*  const [stocks, setStocks] = useState(availableStock); */

		// Function that keeps track of the enrollees for a course.

		/*
			We will refactor the "enroll" function using the "useEffect" hooks to disable the enroll button when the seats reach zero.
		*/
		// Use the disabled state to disable the enroll button.
		// const [disabled, setDisabled] = useState(false);

		// Syntax:
			//useEffect(function, [dependencyArray])

		// function unEnroll(){
		// 	setCount(count - 1);
		// }

		// function enroll(){
		// 	// Activity solution
		// 	// if(seats > 0){
		// 	// 			// 0 + 1
		// 	// 			// setCount(1)
		// 	// 	setCount(count + 1);
		// 	// 	console.log(`Enrollees: ${count}`);
		// 	// 	setSeats(seats - 1);
		// 	// 	console.log(`Seats: ${seats}`);
		// 	// }
		// 	// else{
		// 	// 	alert("No more seats available");
		// 	// }

		// 	setCount(count + 1);
		// 	console.log(`Enrollees: ${count}`);
		// 	setSeats(seats - 1);
		// 	console.log(`Seats: ${seats}`);
		// }

		/* useEffect(()=>{
			if(stocks <= 0){
				//setDisabled(true);
				alert("No more seats available.");
			}
		}, [stocks]); */

	return (
	
	<Container fluid>
		<Row >
			<Col>
				<Card style={{ width: '18rem' }}>
					<Card.Body>
						<Card.Title>{productName}</Card.Title>
						<Card.Text>{description}</Card.Text>
						<Card.Text>Php {price}</Card.Text>
						<Card.Text>{availableStock} available</Card.Text>
						<Button as={Link} to={`/product/camera/${_id}`} variant="secondary">View</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	</Container>

	)
}