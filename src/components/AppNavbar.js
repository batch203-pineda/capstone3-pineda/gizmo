import { useContext, useState, useEffect } from "react"

import { NavLink } from "react-router-dom";
// shorthand method
import { Container, Nav, Navbar } from "react-bootstrap";
import UserContext from "../UserContext";

	

export default function AppNavbar(){

	const { user } = useContext(UserContext);

	const [name, setName] = useState("");
	
	useEffect(() => {
	fetch(`${process.env.REACT_APP_API_URL}/user/details/:userId`, {
		headers:{
				Authorization: `Bearer ${localStorage.getItem("token")}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data.firstName) 
				setName(data.firstName) 
		})
	  },[name])

	  const clearUser = () => {
		setName("")
	  }
	  
	return(
		<Navbar className="py-3 " bg="danger" expand="lg" >
	      <Container fluid>
	        <Navbar.Brand as={ NavLink } to="/" >gizmo</Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
		    {/*
		    	className is use instead of class, to specify a CSS class

		    	ml->ms (margin start)
		    	mr->me (margin end)

				If user is login, nav links visible:
					- Home
					- Product
					- Logout
				If user is not login, nav links visible:
					- Home
					- Product
					- Login
					- Register
			*/}
	          <Nav className="ms-auto">
				<Nav.Link>{name}</Nav.Link>
				<Nav.Link as={ NavLink } to="/" end>Home</Nav.Link>
				{
					(user.isAdmin)
					?
					<Nav.Link as={ NavLink } to="/admindashboard" end>Admin</Nav.Link>
					:
					<Nav.Link as={ NavLink } to="/products" end>Camera</Nav.Link>
				}
				{
					(user.id !== null)
					?
					<Nav.Link as={ NavLink } to="/signout" onClick={clearUser} end>Sign Out</Nav.Link>
					
					:
					<>
					<Nav.Link as={ NavLink } to="/signin" end>Sign In</Nav.Link>
					<Nav.Link as={ NavLink } to="/register" end>Register</Nav.Link>
					</>					
				}		
	            
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>

	)
}