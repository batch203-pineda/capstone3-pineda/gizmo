import { useState, useEffect } from "react";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import { Container } from "react-bootstrap";

import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import Products from "./pages/Products";
import ProductView from "./pages/ProductView";
import Error from "./pages/Error";
import SignIn from "./pages/SignIn";
import Register from "./pages/Register";
import SignOut from "./pages/SignOut";
import AdminDashboard from "./pages/AdminDashboard";
import Footer from "./components/Footer";
import Banner from "./components/Banner";
import Cart from "./pages/Cart";

import './App.css';
import { UserProvider } from "./UserContext";


function App() {

  // Create a "User" state and an "unsetUser" function that will be used in different pages/components within the applicaiton

  // Global state hooks for the user information for validating if a user is logged in.
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  // Function for clearing localStorage
  const unsetUser = () =>{
    // Allow us to clear the information in the localStorage
    localStorage.clear(); 
  }

  useEffect(()=>{
    //console.log(user);
   // console.log(localStorage);
  },[user])

  // To update the User state upon a page load is initiated and a user already exists.
  useEffect(()=>{
    fetch(`${process.env.REACT_APP_API_URL}/user/details/:userId`, {
			headers:{
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

      if(data._id !== undefined){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
      // set back to the initial state of the user if no token found in the localStorage
      else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  },[])



  return (
  
    <UserProvider value={{user, setUser, unsetUser}}>
    {/*Router component is used to wrapped around all components which will have access to the routing system.*/}
    <Router>
      <AppNavbar />
      <Banner />
      <Container>
        {/*Routes holds all our Route components.*/}
        <Routes>
          {/*
            Route assign an endpoint and display the appropriate page component for that endpoint.
              - "exact" and path "props" to assign the endpoint and the page should be only accessed on the specific endpoint
              - "element" props asssigns page components to the displayed endpoint.
          */}
          <Route exact path="/" element={<Home />} />
          <Route exact path="/products" element={<Products />} />
          <Route exact path="/product/camera/:productId" element={<ProductView />} />
          <Route exact path="/checkout/cart" element={<Cart />}/>
          <Route exact path="/signin" element={<SignIn />} />
          <Route exact path="/signout" element={<SignOut />} />
          <Route exact path="/register" element={<Register />} />
          <Route exact path="/admindashboard" element={<AdminDashboard />} />
          {/* "*" is used to render paths that are not found in our routing system.*/}
          <Route exact path="*" element={<Error />} />
        </Routes>
          
      </Container>
      <Footer />
    </Router>
  </UserProvider>


  )
}

export default App;
