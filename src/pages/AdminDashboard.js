import {useContext, useEffect, useState} from "react";

import { Button, Table, Modal, Form } from "react-bootstrap";
import {Navigate} from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function AdminDashboard(){

	const { user } = useContext(UserContext);
    console.log(user);
	// Create allProduct state to contain the products from the response of our fetch data.
	const [allProduct, setAllProduct] = useState([]);

	// State hooks to store the values of the input fields for our modal.
	const [productId, setProductId] = useState("");
	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
    const [availableStock, setAvailableStock] = useState(0);

    // State to determine whether submit button in the modal is enabled or not
    const [isActive, setIsActive] = useState(false);

    // State for Add/Edit Modal
    const [showAdd, setShowAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);

	// To control the add product modal pop out
	const openAdd = () => setShowAdd(true); //Will show the modal
	const closeAdd = () => setShowAdd(false); //Will hide the modal

	// To control the edit product modal pop out
	// We have passed a parameter from the edit button so we can retrieve a specific product and bind it with our input fields.
	const openEdit = (id) => {
		setProductId(id);

		// Getting a specific product to pass on the edit modal
		fetch(`${ process.env.REACT_APP_API_URL }/product/camera/${id}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			// updating the product states for editing
			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
			setAvailableStock(data.availableStock);
		});

		setShowEdit(true)
	};

	const closeEdit = () => {

		// Clear input fields upon closing the modal
	    setProductName('');
	    setDescription('');
	    setPrice(0);
	    setAvailableStock(0);
		setShowEdit(false);
	};

	// [SECTION] To view all product in the database (active & inactive)
	// fetchData() function to get all the active/inactive products.
	const fetchData = () =>{
		// get all the products from the database
		fetch(`${process.env.REACT_APP_API_URL}/product/stock`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProduct(data.map(product => {
				return (
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.productName}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.availableStock}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								// conditonal rendering on what button should be visible base on the status of the product
								(product.isActive)
								?
									<Button variant="danger" size="sm" onClick={() => archive(product._id, product.name)}>Archive</Button>
								:
									<>
										<Button variant="success" size="sm" className="mx-1" onClick={() => unarchive(product._id, product.prodcutName)}>Unarchive</Button>
										<Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(product._id)}>Edit</Button>
									</>

							}
						</td>
					</tr>
				)
			}));
		});
	}

	// to fetch all product in the first render of the page.
	useEffect(()=>{
		fetchData();
	}, [])

	// [SECTION] Setting the product to Active/Inactive

	// Making the product inactive
	const archive = (id, productName) =>{
		console.log(id);
		console.log(productName);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/product/archive/${id}`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Archive Successful",
					icon: "success",
					text: `${productName} is now inactive.`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// Making the product active
	const unarchive = (id, productName) =>{
		console.log(id);
		console.log(productName);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/product/archive/${id}`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Unarchive Successful",
					icon: "success",
					text: `${productName} is now active.`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// [SECTION] Adding a new product
	// Inserting a new product in our database
	const addProduct = (e) =>{
			// Prevents page redirection via form submission
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/product/addproduct`, {
		    	method: "POST",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    productName: productName,
				    description: description,
				    price: price,
				    availableStock: availableStock
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Product succesfully Added",
		    		    icon: "success",
		    		    text: `${productName} is now added`
		    		});

		    		// To automatically add the update in the page
		    		fetchData();
		    		// Automatically closed the modal
		    		closeAdd();
		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});
		    		closeAdd();
		    	}

		    })

		    // Clear input fields
		    setProductName('');
		    setDescription('');
		    setPrice(0);
		    setAvailableStock(0);
	}

	// [SECTION] Edit a specific product
	// Updating a specific product in our database
	// edit a specific product
	const editProduct = (e) =>{
			// Prevents page redirection via form submission
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/product/camera/${productId}`, {
		    	method: "PUT",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    productName: productName,
				    description: description,
				    price: price,
				    availableStock: availableStock
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Product succesfully Updated",
		    		    icon: "success",
		    		    text: `${productName} is now updated`
		    		});

		    		// To automatically add the update in the page
		    		fetchData();
		    		// Automatically closed the form
		    		closeEdit();

		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});

		    		closeEdit();
		    	}

		    })

		    // Clear input fields
		    setProductName('');
		    setDescription('');
		    setPrice(0);
		    setAvailableStock(0);
	} 

	// Submit button validation for add/edit product
	useEffect(() => {

        // Validation to enable submit button when all fields are populated and set a price and slot greater than zero.
        if(productName !== "" && description !== "" && price > 0 && availableStock > 0){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [productName, description, price, availableStock]);

	return(
		(user.isAdmin)
		?
		<>
			{/*Header for the admin dashboard and functionality for create product and show enrollments*/}
			<div>
				<h1>Admin Dashboard</h1>
				{/*Adding a new product */}
				<Button variant="success" className="
				mx-2" onClick={openAdd}>Add Product</Button>
				{/*To view all the user enrollments*/}
				<Button variant="secondary" className="
				mx-2">Show User Order</Button>
			</div>
			{/*End of admin dashboard header*/}

			{/*For view all the products in the database.*/}
			<Table striped bordered hover>
		      <thead>
		        <tr>
		          <th>Product ID</th>
		          <th>Product Name</th>
		          <th>Description</th>
		          <th>Price</th>
		          <th>Available Stock</th>
		          <th>Status</th>
		          <th>Actions</th>
		        </tr>
		      </thead>
		      <tbody>
	        	{allProduct}
		      </tbody>
		    </Table>
			{/*End of table for product viewing*/}

	    	{/*Modal for Adding a new product*/}
	        <Modal show={showAdd} fullscreen={true} onHide={closeAdd}>
	    		<Form onSubmit={e => addProduct(e)}>

	    			<Modal.Header closeButton>
	    				<Modal.Title>Add New Product</Modal.Title>
	    			</Modal.Header>

	    			<Modal.Body>
	    	        	<Form.Group controlId="name" className="mb-3">
	    	                <Form.Label>Product Name</Form.Label>
	    	                <Form.Control 
	    		                type="text" 
	    		                placeholder="Enter Product Name" 
	    		                value = {productName}
	    		                onChange={e => setProductName(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="description" className="mb-3">
	    	                <Form.Label>Product Description</Form.Label>
	    	                <Form.Control
	    	                	as="textarea"
	    	                	rows={3}
	    		                placeholder="Enter Product Description" 
	    		                value = {description}
	    		                onChange={e => setDescription(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="price" className="mb-3">
	    	                <Form.Label>Product Price</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Product Price" 
	    		                value = {price}
	    		                onChange={e => setPrice(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="slots" className="mb-3">
	    	                <Form.Label>Available Stock</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter product Slots" 
	    		                value = {availableStock}
	    		                onChange={e => setAvailableStock(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>
	    			</Modal.Body>

	    			<Modal.Footer>
	    				{ isActive 
	    					? 
	    					<Button variant="primary" type="submit" id="submitBtn">
	    						Save
	    					</Button>
	    				    : 
	    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
	    				    	Save
	    				    </Button>
	    				}
	    				<Button variant="secondary" onClick={closeAdd}>
	    					Close
	    				</Button>
	    			</Modal.Footer>

	    		</Form>	
	    	</Modal>
	    {/*End of modal for adding product*/}

    	{/*Modal for Editing a product*/}
        <Modal show={showEdit} fullscreen={true} onHide={closeEdit}>
    		<Form onSubmit={e => editProduct(e)}>

    			<Modal.Header closeButton>
    				<Modal.Title>Edit a Product</Modal.Title>
    			</Modal.Header>

    			<Modal.Body>
    	        	<Form.Group controlId="name" className="mb-3">
    	                <Form.Label>Product Name</Form.Label>
    	                <Form.Control 
    		                type="text" 
    		                placeholder="Enter Product Name" 
    		                value = {productName}
    		                onChange={e => setProductName(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="description" className="mb-3">
    	                <Form.Label>Product Description</Form.Label>
    	                <Form.Control
    	                	as="textarea"
    	                	rows={3}
    		                placeholder="Enter Product Description" 
    		                value = {description}
    		                onChange={e => setDescription(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="price" className="mb-3">
    	                <Form.Label>Product Price</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Product Price" 
    		                value = {price}
    		                onChange={e => setPrice(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="slots" className="mb-3">
    	                <Form.Label>Available Stock</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Stock" 
    		                value = {availableStock}
    		                onChange={e => setAvailableStock(e.target.value)}
    		                required
    	                />
    	            </Form.Group>
    			</Modal.Body>

    			<Modal.Footer>
    				{ isActive 
    					? 
    					<Button variant="primary" type="submit" id="submitBtn">
    						Save
    					</Button>
    				    : 
    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
    				    	Save
    				    </Button>
    				}
    				<Button variant="secondary" onClick={closeEdit}>
    					Close
    				</Button>
    			</Modal.Footer>

    		</Form>	
    	</Modal>
    	{/*End of modal for editing a product*/}
		</>
		:
		<Navigate to="/camera" />
	)
}