import { useEffect, useState, useContext } from "react";

import { Navigate } from "react-router-dom";

import ProductCard from "../components/ProductCard";
import UserContext from "../UserContext";


export default function Products() {

	const { user } = useContext(UserContext);

	// couses state that will be used to store the products retrieve in the database.

	const [products, setProducts] = useState([]);

	useEffect(() =>{
		// Will retrieve all the active products
		fetch(`${process.env.REACT_APP_API_URL}/product/collection`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product =>{
				return(
					<ProductCard key={product._id} productProp={product}/>
				);
			}));
		})
	}, []);

	// To confirm if we have import our mock database.
	// console.log(coursesData);

	// console.log(coursesData[0]);
	// The curly braces ({}) are used for props to signify that we are providing information using JavaScript expressions.

	// Everytime the map method loops through the data, it creates "CourseCard" component and then passes the current element in our coursesDara array using courseProp.
	// Multiple components created through the map method maust have a unique key that will help ReactJS identify which components/elements have been changed, added, or removed.
	// const courses = coursesData.map(course =>{
	// 	return(
	// 		<CourseCard key={course.id} courseProp={course}/>
	// 	);
	// });


	return(
		(user.isAdmin)
		?
			<Navigate to="/admindashboard" />
		:	
		<>
			<h1>Camera</h1>
			<div>
				{products}
			</div>
		</>
	)
}