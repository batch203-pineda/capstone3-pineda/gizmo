import { Navigate } from "react-router-dom";
import { useEffect, useContext } from "react";

import UserContext from "../UserContext";

export default function SignOut(){

	const { unsetUser, setUser } = useContext(UserContext);

	unsetUser();

	useEffect(() =>{
		setUser({
			id: null,
			isAdmin: null
		});
	});

	// Redirect back to login
	return(
		<Navigate to="/" />
	)
}
