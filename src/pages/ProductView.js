import { useState, useEffect, useContext } from "react";
import {Container, Card, Button, Row, Col} from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
//import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function ProductView(){

	const { user } = useContext(UserContext);

	// "useParams" hooks allows us to retrieve the productId passed via the URL.

	const { productId } = useParams();

	const navigate = useNavigate();

	// Create state hooks to capture the information of a specific product and display it in our application.
	const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	useEffect(()=> {
		console.log(productId);

		fetch(`${ process.env.REACT_APP_API_URL }/product/camera/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price);

		});

	}, [productId])

	/* const enroll = (productId) =>{

		fetch(`${ process.env.REACT_APP_API_URL }/users/enroll`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data){
				Swal.fire({
					title: "Successfully Enrolled",
					icon: "success",
					text: "You have successfully enrolled for this product."
				});

				navigate("/products");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}

		});

	} */

	return(
		<Container>
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{productName}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							
							{
								(user.id !== null)
								?
									<Button as={Link} to="/checkout/cart" variant="secondary" size="lg" >Add to Cart</Button>//onClick={() => enroll(productId)}
								:
									<Button as={Link} to="/signin" variant="secondary"  size="lg">SignIn to Buy</Button>
							}
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}