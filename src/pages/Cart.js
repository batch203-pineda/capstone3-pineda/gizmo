import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Container } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';




export default function Cart() {
    return (
    <>
    <Container className="py-5">
    <Card>
        <Card.Body>
        <h1 className="text-center">ORDER SUMMARY</h1>
    <Table striped>
        <thead>
            <tr>
                <th>
                    Product Name
                </th>
                <th>
                    Quantity
                </th>
                <th>
                    Price
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>FETCH PRODUCTNAME HERE!</td>
                <td>
                <InputGroup className="mb-3" size="sm">
                <Button variant="outline-secondary" id="button-addon2">-</Button>
                <InputGroup.Text>0</InputGroup.Text>
                <Button variant="outline-secondary" id="button-addon2">+</Button>
                </InputGroup>
                
                </td>
                <td>FETCH PRICE HERE!</td>
                <td><Button variant="outline-danger">Remove</Button></td>
            </tr>
        </tbody>
    </Table>
            <hr></hr>
          <Card.Text>
            TOTAL: ₱ 15000
          </Card.Text>
        </Card.Body>
        <Card.Footer >
            <Button  variant="danger">Check Out</Button>
            <Button  variant="danger">Continue Shopping</Button>
        </Card.Footer>
      </Card>
      </Container>
      
    
    
    
    </>
    )
}

