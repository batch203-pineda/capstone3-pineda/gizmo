import { Link } from "react-router-dom";
import { Row, Col, Button } from "react-bootstrap";


export default function Error() {
    return(
            <Row>
			<Col className="p-5 text-center">
				<h1>404 - Not found</h1>
            	<p>The page you are looking for cannot be found</p>
				<Button as={ Link } to="/" variant="primary">Back to Home</Button>
			</Col>
		</Row>

    )
}