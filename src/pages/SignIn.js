import { useState, useEffect, useContext } from 'react';

import { Navigate } from "react-router-dom";
import { Form, Button } from 'react-bootstrap';
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function SignIn() {

	// Allow us to consume the User Context object and its values(properties) to use for user validation.
	const { user, setUser } = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Allow us to gain access to methods that allow us to redirect to another page.
	//const navigate = useNavigate();

	useEffect(() => {

	    // Validation to enable submit button when all fields are populated
	    if(email !== '' && password !== ''){
	        setIsActive(true);
	    }else{
	        setIsActive(false);
	    }

	}, [email, password]);

	// Function to simulate user login
	function authenticate(e) {

		// Prevents page redirection via form submission
	    e.preventDefault();

		/*

			Syntax:
			fetch("url", {options})
			.then(res => res.json())
			.then(data => {})

			/users
			/courses
		*/

		fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data.accessToken);
			// If no user information is found, the "accessToken" property will not be available and will return undefined
			if(data.accessToken !== undefined){
				// The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
				localStorage.setItem("token", data.accessToken);
				retrieveUserDetails(data.accessToken);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: `Welcome to Gizmo!`
                });

			}else {
                Swal.fire({
                    title: "Authentication Failed!",
                    icon: "error",
                    text: "Check your login details and try again."
                })
            }
		})

	    // "localStorage" is a property that allows JavaScript sites and application to save key-value pairs in a web browser with no expiration date.
	    // Syntax:
	    	// localStorage.setItem("propertyName", value);
	    // localStorage.setItem("email", email);

	    // setUser({
	    // 	email: localStorage.getItem("email")
	    // });

	    // Clear input fields after submission
	    setEmail('');
	    setPassword('');

	    // alert(`${email} has been verified! Welcome back!`);

	    // Redirect us to home page
	    //navigate("/");

	}

	const retrieveUserDetails = (token) => {
		// Token will be sent as part of the request's header.

		fetch(`${process.env.REACT_APP_API_URL}/user/details/:userId`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Change the global "user" state to store the "id" and "isAdmin" property property of the user which will be used for validation across the whole application
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			});
		})
	}

	return (
		// Create a conditional statement that will redirect the user to the course page when a user is already logged in.
		(user.id !== null)
		?
			<Navigate to="/" />
		:
		<>
			<h1 className="my-5 text-center">Sign In</h1>
			<Form onSubmit={(e) => authenticate(e)}>
				<Form.Group controlId="userEmail" className="mb-3">
					<Form.Label>Email</Form.Label>
					<Form.Control 
		                type="email" 
		                placeholder="Enter email"
		                value={email}
		    			onChange={(e) => setEmail(e.target.value)}
		                required
					/>
				</Form.Group>

				<Form.Group controlId="password" className="mb-3">
					<Form.Label>Password</Form.Label>
					<Form.Control 
					    type="password" 
		                placeholder="Password"
		                value={password}
		    			onChange={(e) => setPassword(e.target.value)}
		                required
					/>
				</Form.Group>

			  {
			  	isActive
			  	?
				  	<Button variant="primary" type="submit" id="submitBtn">
				   	 Login
				  	</Button>
			  	:
			  		<Button variant="danger" type="submit" id="submitBtn" disabled>
				   	 Login
				  	</Button>
			  }
			</Form>
		</>
	)
}
